package com.vittorio.herencia;

import com.vittorio.herencia.animales.Gallina;
import com.vittorio.herencia.animales.Gato;

public class Main {

	public static void main(String[] args) {
		
		Gato michi = new Gato(7, null, 3, 4, false, 800, "Felino");	
		Gallina cocode = new Gallina(3, null, 1, 2, true, 4000, "Pajaros");
		
		michi.eat(cocode);
		cocode.eat(michi);

	}

} 


