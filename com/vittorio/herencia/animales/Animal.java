package com.vittorio.herencia.animales;

import com.vittorio.herencia.interfaces.Comestible;

public class Animal {
	
	private String nickName;
	private String name;
	private int patas;
	private final boolean vuela;
	private int velocidad;
	private float peso;
	private String especie;
	
	
	public Animal(int patas, boolean vuela, float peso, String especie) {
		this.patas = patas;
		this.vuela = vuela;
		this.peso = peso;
		this.especie = especie;
	}
	
	
	
	public String getNickName() {
		return nickName;
	}



	public void setNickName(String nickName) {
		this.nickName = nickName;
	}



	public void eat(Comestible elBocadito) {
		System.out.println("Estoy comiendo" + elBocadito.toString());
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean vuela() {
		return vuela;
	}


	public int getPatas() {
		return patas;
	}
	public void setPatas(int patas) {
		this.patas = patas;
	}
	
	public int getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	
	

}
