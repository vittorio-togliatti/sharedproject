package com.vittorio.herencia.animales;

import java.util.List;

import com.vittorio.herencia.common.Vacuna;
import com.vittorio.herencia.interfaces.Comestible;

public class Gato extends Animal implements Comestible{
	
	private float largoBigotes;
	private int numVidas;
	private List<Vacuna> vacunas;

	
	
	public Gato(int numVidas, List<Vacuna> vacunas, float largoBigotes, int patas, boolean vuela, float peso, String especie) {
		super(patas, vuela, peso, especie);
		
		this.largoBigotes = largoBigotes;
		this.numVidas = numVidas;
		this.vacunas = vacunas;
		
	}
	
	



	public float getLargoBigotes() {
		return largoBigotes;
	}



	public void setLargoBigotes(float largoBigotes) {
		this.largoBigotes = largoBigotes;
	}



	public int getNumVidas() {
		return numVidas;
	}



	public void setNumVidas(int numVidas) {
		this.numVidas = numVidas;
	}



	public List<Vacuna> getVacunas() {
		return vacunas;
	}



	public void setVacunas(List<Vacuna> vacunas) {
		this.vacunas = vacunas;
	}
	
	@Override
	public void sayIsCooked() {
		// TODO Auto-generated method stub
		
	}

}
