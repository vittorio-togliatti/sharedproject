package com.vittorio.herencia.animales;

import java.util.List;

import com.vittorio.herencia.common.Vacuna;
import com.vittorio.herencia.interfaces.Comestible;

public class Gallina extends Animal implements Comestible{
	
	private int inteligenciaNegativa;
	private int numHuevos;
	private List<Vacuna> vacunas;

	
	
	public Gallina(int numHuevos, List<Vacuna> vacunas, int inteligenciaNegativa, int patas, boolean vuela, float peso, String especie) {
		super(patas, vuela, peso, especie);
		
		this.inteligenciaNegativa = inteligenciaNegativa;
		this.numHuevos = numHuevos;
		this.vacunas = vacunas;
		
	}



	public int getInteligenciaNegativa() {
		return inteligenciaNegativa;
	}



	public void setInteligenciaNegativa(int inteligenciaNegativa) {
		this.inteligenciaNegativa = inteligenciaNegativa;
	}



	public int getNumHuevos() {
		return numHuevos;
	}



	public void setNumHuevos(int numHuevos) {
		this.numHuevos = numHuevos;
	}



	public List<Vacuna> getVacunas() {
		return vacunas;
	}



	public void setVacunas(List<Vacuna> vacunas) {
		this.vacunas = vacunas;
	}



	@Override
	public void sayIsCooked() {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
