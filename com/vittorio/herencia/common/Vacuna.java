package com.vittorio.herencia.common;

public class Vacuna {
	
	public Vacuna(String nombre) {
		this.nombre = nombre;
	}
	
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
